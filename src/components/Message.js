import React, { useEffect, useState } from "react";
import { auth, db } from "../firebase";
import { deleteDoc, doc } from "firebase/firestore";

const Message = ({ message }) => {
  const currentUser = auth.currentUser;
  const [canRemove, setCanRemove] = useState(false);

  const handleRemoveMessage = async () => {
    if (currentUser.uid === message.uid) {
      try {
        await deleteDoc(doc(db, "messages", message.id));
        console.log("Message removed successfully!");
      } catch (error) {
        console.error("Error removing message:", error);
      }
    }
  };

  useEffect(() => {
    const currentTime = new Date().getTime();
    const messageTime = message.createdAt?.toDate().getTime();
    const timeDifference = currentTime - messageTime;
    const timeLimit = 10 * 1000; // 10 seconds

    if (timeDifference <= timeLimit) {
      setCanRemove(true);
      const timer = setTimeout(() => {
        setCanRemove(false);
      }, timeLimit - timeDifference);
      return () => clearTimeout(timer);
    } else {
      setCanRemove(false);
    }
  }, [message]);

  useEffect(() => {
    if (canRemove && message.uid === currentUser.uid) {
      const timer = setTimeout(() => {
        setCanRemove(false);
      }, 10000); // 10 seconds
      return () => clearTimeout(timer);
    }
  }, [canRemove, message, currentUser]);

  return (
    <div className={`chat-bubble ${message.uid === currentUser.uid ? "right" : ""}`}>
      {canRemove && message.uid === currentUser.uid && (
        <button
          onClick={handleRemoveMessage}
          className={`remove-message-button ${canRemove ? "removable" : ""}`}
        >
          🗑️
        </button>
      )}
      <img className="chat-bubble__left" src={message.avatar} alt="user avatar" />
      <div className="chat-bubble__right">
        <p className="user-name">{message.name}</p>
        <p className="user-message">{message.text}</p>
        {message.image && (
          <img className="chat-bubble__image" src={message.image} alt="message image" />
        )}
      </div>
    </div>
  );
};

export default Message;
