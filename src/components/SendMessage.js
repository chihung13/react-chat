import React, { useState, useRef } from "react";
import { auth, db, storage } from "../firebase";
import { addDoc, collection, serverTimestamp } from "firebase/firestore";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";

const SendMessage = ({ scroll }) => {
  const [message, setMessage] = useState("");
  const [image, setImage] = useState(null);
  const fileInputRef = useRef(null);
  const imagePreviewRef = useRef(null);

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setImage(file);
        imagePreviewRef.current.src = reader.result;
      };
      reader.readAsDataURL(file);
    }
  };

  const sendMessage = async (event) => {
    event.preventDefault();

    if (message.trim() === "" && !image) {
      console.log("Enter valid message or select an image");
      return;
    }

    const { uid, displayName, photoURL } = auth.currentUser;

    let imageUrl = null; // Initialize imageUrl to null

    if (image) {
      const imageRef = ref(storage, `images/${Date.now()}-${image.name}`);
      const uploadTask = uploadBytesResumable(imageRef, image);

      uploadTask.on(
        "state_changed",
        (snapshot) => {},
        (error) => {
          console.log(error);
        },
        async () => {
          try {
            imageUrl = await getDownloadURL(uploadTask.snapshot.ref);
            // The image URL is obtained successfully here.
            console.log("Image uploaded successfully!");
            console.log("Image URL:", imageUrl);
            // Now that the imageUrl is available, add the document
            await addDoc(collection(db, "messages"), {
              text: message,
              name: displayName,
              avatar: photoURL,
              image: imageUrl, // Use the obtained imageUrl
              createdAt: serverTimestamp(),
              uid,
            });
            // Reset the file input field and the image state
            fileInputRef.current.value = "";
            setImage(null);
            imagePreviewRef.current.src = null;
          } catch (error) {
            console.log(error);
          }
        }
      );
    } else {
      // If no image is selected, directly add the document
      await addDoc(collection(db, "messages"), {
        text: message,
        name: displayName,
        avatar: photoURL,
        image: null, // Set image to null explicitly
        createdAt: serverTimestamp(),
        uid,
      });
    }

    setMessage("");
    scroll.current.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <div>
      {/* Image thumbnail */}
      <div className={`img-thumbnail ${image ? "has-image" : ""}`}>
        <span className="remove-image">🗙</span>
        <img ref={imagePreviewRef} src={null} alt="Preview" />
      </div>

      <form onSubmit={sendMessage} className="send-message">
        <label htmlFor="messageInput" hidden>
          Enter Message
        </label>

        <input
          id="messageInput"
          name="messageInput"
          type="text"
          placeholder="Enter message..."
          value={message}
          onChange={(e) => setMessage(e.target.value)}
        />

        <input
          hidden
          id="upload"
          ref={fileInputRef}
          type="file"
          onChange={handleImageChange}
        />
        <label htmlFor="upload">📷</label>

        <button type="submit">Send</button>
      </form>
    </div>
  );
};

export default SendMessage;
