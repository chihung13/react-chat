// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage"; // import storage

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAEZGNSmSfF_NdsXuOkiys2k2YRpWkCVjQ",
  authDomain: "my-create-chat-ap.firebaseapp.com",
  databaseURL: "https://my-create-chat-ap-default-rtdb.firebaseio.com",
  projectId: "my-create-chat-ap",
  storageBucket: "my-create-chat-ap.appspot.com",
  messagingSenderId: "1063310056287",
  appId: "1:1063310056287:web:e28520af741ce169c674ea",
  measurementId: "G-DMW9WGVGPK"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getFirestore(app);

// Export storage
export const storage = getStorage(app);
